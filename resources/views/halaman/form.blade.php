<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>Register</title>
</head>

<body>

    <div class="col-lg-7">
        <div class="m-20">
            <h1>Buat Account Baru!</h1>

            <h4>Sign Up Form</h4>
            <div class="row">
                <div class class="col-lg">
                    <form action="/welcome" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">First Name</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="First Name" name="namadepan">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Last Name</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Last Name" name="namabelakang">
                        </div>
                        <label for="">Gender</label>
                        <div class="form-check">
                            <label class="form-check-label" for="flexCheckDefault">
                                Male
                            </label>
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        </div>
                        <div class="form-check">
                            <label class="form-check-label" for="flexCheckDefault">
                                Female
                            </label>
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        </div>
                        <div class="form-check">
                            <label class="form-check-label" for="flexCheckDefault">
                                Others
                            </label>
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        </div>
                        <label for="">Nationality</label>
                        <select class="form-select mt-3" aria-label="Default select example">
                            <option selected>Indonesia</option>
                            <option value="1">Malaysia</option>
                            <option value="2">Singapore</option>
                            <option value="3">Japan</option>
                        </select>
                        <label for="">Language Spoken</label>
                        <select class="form-select mt-3" aria-label="Default select example">
                            <option selected>Indonesia</option>
                            <option value="1">English</option>
                            <option value="2">Other</option>
                        </select>
                        <div class="mb-3 mt-3">
                            <label for="exampleFormControlTextarea1" class="form-label">Bio</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
</body>

</html>
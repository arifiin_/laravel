<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('halaman.form');
    }

    public function kirim(Request $request)
    {
        // return view('halaman.home');
        // dd($request->all());
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];
        return view('halaman.home', compact('namadepan', 'namabelakang'));
    }
}
